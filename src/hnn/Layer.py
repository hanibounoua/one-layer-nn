import numpy as np
import cupy as cp


class Layer:

    def __init__(self, n_unit, input_shape, activation, xp=cp, init_rate=.001):
        self.n_unit = n_unit
        self.input_shape = input_shape
        self.activation = activation

        self.omega = xp.random.randn((n_unit, input_shape)) * init_rate
        self.b = xp.zeros((n_unit, 1))
    
    